/** \file       Config.h
 *  \brief      Keeps cofiguration constants.
 *  \author     Nemanja Paripovic
 *  \date       10.12.2021.
 **/

#pragma once

#include "opencv2/core.hpp"
#include <string>
#include <vector>

const std::string faceProto =
    "/home/nparipovic/Documents/FTN/Diplomski/FaceDetectionProject/resources/opencv_face_detector.pbtxt";
const std::string faceModel =
    "/home/nparipovic/Documents/FTN/Diplomski/FaceDetectionProject/resources/opencv_face_detector_uint8.pb";

const std::string ageProto =
    "/home/nparipovic/Documents/FTN/Diplomski/FaceDetectionProject/resources/age_deploy.prototxt";
const std::string ageModel =
    "/home/nparipovic/Documents/FTN/Diplomski/FaceDetectionProject/resources/age_net.caffemodel";

const std::string genderProto =
    "/home/nparipovic/Documents/FTN/Diplomski/FaceDetectionProject/resources/gender_deploy.prototxt";
const std::string genderModel =
    "/home/nparipovic/Documents/FTN/Diplomski/FaceDetectionProject/resources/gender_net.caffemodel";

const cv::Scalar MODEL_MEAN_VALUES = cv::Scalar(104, 117, 123);

const std::vector<std::string> ageList =
    {"(0-2)", "(4-6)", "(8-12)", "(15-20)", "(25-32)", "(38-43)", "(48-53)", "(60-100)"};

const std::vector<std::string> genderList = {"Musko", "Zensko"};

const int padding = 20;

const double THRESHOLD = 0.7;
