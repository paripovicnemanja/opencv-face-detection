/** \file       FaceHandler.h
 *  \brief      Declaration of the FaceHandler class.
 *  \author     Nemanja Paripovic
 *  \date       10.12.2021.
 **/

#pragma once

// Library include
#include "opencv4/opencv2/core.hpp"
#include "opencv4/opencv2/dnn/dnn.hpp"
#include "opencv4/opencv2/highgui.hpp"
#include "opencv4/opencv2/imgproc.hpp"

// STL includes
#include <tuple>
#include <vector>

class FaceHandler {
  public:
    /**
     * @brief Constructor
     */
    FaceHandler();

    /**
     * @brief Destructor
     */
    ~FaceHandler();

    /**
     * @brief Initializes neural network objects for face detection, age and gender prediction
     * @return @li true - On success
     *         @li false - On failure
     */
    bool InitializeNetworks();

    /**
     * @brief Fuction that detects faces form photo
     * @param frame[in] - Input photo used for face detection
     * @param conf_threshold [in] - Threshold value which defines that photo is successfully detected
     * @return tuple object which contains
     */
    std::tuple<cv::Mat, std::vector<std::vector<int>>> DetectFaces(cv::Mat& frame, double conf_threshold);

    /**
     * @brief Predicts age of the detected face
     * @param blob [in] - Input blob
     * @return @li 0-2
     *         @li 4-6
     *         @li 8-12
     *         @li 15-20
     *         @li 25-32
     *         @li 38-43
     *         @li 48-53
     *         @li 60-100
     */
    const std::string PredictAge(cv::Mat blob);

    /**
     * @brief Predicts gender of the detected face
     * @param blob [in] - Input blob
     * @return @li Muski
     *         @li Zenski
     */
    const std::string PredictGender(cv::Mat blob);

  private:
    /**
     * @brief Neural network object for face detection
     */
    cv::dnn::Net m_faceNet;

    /**
     * @brief Neural network object for age prediction
     */
    cv::dnn::Net m_ageNet;

    /**
     * @brief Neural network object for gender prediction
     */
    cv::dnn::Net m_genderNet;
};
