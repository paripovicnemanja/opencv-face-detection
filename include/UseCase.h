/** \file       UseCase.h
 *  \brief      Declaration of the UseCase class.
 *  \author     Nemanja Paripovic
 *  \date       10.12.2021.
 **/

#pragma once

#include "FaceHandler.h"

#include "opencv4/opencv2/core.hpp"

class UseCase {
  public:
    /**
     * @brief Constructor
     */
    UseCase();

    /**
     * @brief Destructor
     */
    ~UseCase();

    /**
     * @brief Initializes use case object
     * @return @li true - On success
     *         @li false - On failure
     */
    bool Initialize();

    /**
     * @brief Executes use case. Depending on parameter forwared to the function use case implies only face detection,
     *        or face detection together with age and gender prediction
     * @param frame [in] - Represents input frame used for use case
     * @param onlyFaceDetection [in] - Determines if use case should contain only face detection or face detection
     *        together with age and gender prediction
     */
    void ExecuteUseCase(cv::Mat frame, bool onlyFaceDetection = false);

  private:
    /**
     * @brief Check boundaries for drawn rectangle around face
     * @param frame [in] - Input frame which contains detected face
     * @param box [in/out] - Object which contains coordinates of detected face.
     */
    void CheckBoundaries(const cv::Mat frame, std::vector<int>& box);

    /**
     * @brief ShowImage
     * @param frame
     */
    void ShowImage(const cv::Mat frame);

    /**
     * @brief Object which handles all face related function
     */
    FaceHandler m_faceHandler;
};
