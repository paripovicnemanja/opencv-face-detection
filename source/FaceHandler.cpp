/** \file       FaceHandler.h
 *  \brief      Definition of the FaceHandler class.
 *  \author     Nemanja Paripovic
 *  \date       10.12.2021.
 **/

#include "FaceHandler.h"
#include "Config.h"

using namespace cv;
using namespace cv::dnn;
using namespace std;

FaceHandler::FaceHandler() {
}

FaceHandler::~FaceHandler() {
}

bool FaceHandler::InitializeNetworks() {
    if (ageModel.empty() || ageProto.empty() || genderModel.empty() || genderProto.empty() || faceModel.empty() ||
        faceProto.empty()) {
        return false;
    }

    m_ageNet = cv::dnn::readNet(ageModel, ageProto);
    m_genderNet = cv::dnn::readNet(genderModel, genderProto);
    m_faceNet = cv::dnn::readNet(faceModel, faceProto);

    return true;
}

std::tuple<cv::Mat, std::vector<std::vector<int>>> FaceHandler::DetectFaces(Mat& frame, double conf_threshold) {
    Mat frameOpenCVDNN = frame.clone();
    int frameHeight = frameOpenCVDNN.rows;
    int frameWidth = frameOpenCVDNN.cols;

    const double inScaleFactor = 1.0;
    const Size size = Size(227, 227);

    cv::Mat inputBlob = cv::dnn::blobFromImage(frameOpenCVDNN, inScaleFactor, size, MODEL_MEAN_VALUES, true, false);

    m_faceNet.setInput(inputBlob, "data");
    cv::Mat detection = m_faceNet.forward("detection_out");

    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

    vector<vector<int>> bboxes;

    for (int i = 0; i < detectionMat.rows; i++) {
        float confidence = detectionMat.at<float>(i, 2);

        if (confidence > conf_threshold) {
            int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frameWidth);
            int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frameHeight);
            int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frameWidth);
            int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frameHeight);
            vector<int> box = {x1, y1, x2, y2};
            bboxes.push_back(box);
            cv::rectangle(frameOpenCVDNN, cv::Point(x1, y1), cv::Point(x2, y2), cv::Scalar(0, 255, 0), 2, 4);
        }
    }

    return make_tuple(frameOpenCVDNN, bboxes);
}

const string FaceHandler::PredictAge(Mat blob) {
    m_genderNet.setInput(blob);

    // string gender_preds;
    vector<float> genderPreds = m_genderNet.forward();

    // find max element index
    // distance function does the argmax() work in C++
    int max_index_gender = std::distance(genderPreds.begin(), max_element(genderPreds.begin(), genderPreds.end()));
    string gender = genderList[max_index_gender];

    return gender;
}

const string FaceHandler::PredictGender(cv::Mat blob) {
    m_ageNet.setInput(blob);

    vector<float> agePreds = m_ageNet.forward();

    // finding maximum indicd in the age_preds vector
    int max_indice_age = std::distance(agePreds.begin(), max_element(agePreds.begin(), agePreds.end()));
    string age = ageList[max_indice_age];

    return age;
}
