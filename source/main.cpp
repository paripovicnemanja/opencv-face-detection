/** \file       main.cpp
 *  \brief      Main file
 *  \author     Nemanja Paripovic
 *  \date       10.12.2021.
 **/

#include "Config.h"
#include "FaceHandler.h"
#include "UseCase.h"

int main(int argc, char* argv[]) {

    // Initialize UseCase object
    UseCase useCase;
    useCase.Initialize();

    // Read input photo or video
    cv::VideoCapture cap;
    if (argc > 1) {
        cap.open(argv[1]);
    } else {
        cap.open(0);
    }

    while (cv::waitKey(1) < 0) {
        // read frame
        cv::Mat frame;
        cap.read(frame);

        if (frame.empty()) {
            cv::waitKey();
            break;
        }
        // Run use case
        useCase.ExecuteUseCase(frame);
    }
}
