/** \file       UseCase.h
 *  \brief      Definition of the UseCase class.
 *  \author     Nemanja Paripovic
 *  \date       10.12.2021.
 **/

#include "UseCase.h"
#include "Config.h"

#include <vector>

using namespace std;
using namespace cv;
using namespace cv::dnn;

UseCase::UseCase() {
}

UseCase::~UseCase() {
}

bool UseCase::Initialize() {
    return m_faceHandler.InitializeNetworks();
}

void UseCase::ExecuteUseCase(cv::Mat frame, bool onlyFaceDetection) {

    // Face detection
    vector<vector<int>> faceBoxes;
    Mat frameFace;
    tie(frameFace, faceBoxes) = m_faceHandler.DetectFaces(frame, THRESHOLD);

    if (!onlyFaceDetection) {

        for (auto it = begin(faceBoxes); it != end(faceBoxes); it++) {

            CheckBoundaries(frame, *it);

            Rect rec(it->at(0) - padding,
                it->at(1) - padding,
                it->at(2) - it->at(0) + 2 * padding,
                it->at(3) - it->at(1) + 2 * padding);

            // take the ROI of box on the frame
            Mat face = frame(rec);

            // Predprocess photo
            Mat blob = blobFromImage(face, 1, Size(227, 227), MODEL_MEAN_VALUES, false);

            const std::string age = m_faceHandler.PredictAge(blob);

            const std::string gender = m_faceHandler.PredictGender(blob);

            string label = gender + ", " + age;

            cv::putText(frameFace,
                label,
                Point(it->at(0), it->at(1) - padding),
                cv::FONT_HERSHEY_SIMPLEX,
                0.9,
                Scalar(0, 255, 255),
                2,
                cv::LINE_AA);
        }
    }

    ShowImage(frameFace);
}

void UseCase::CheckBoundaries(const Mat frame, std::vector<int>& box) {
    // Points in vector are organized in following order:
    // (x1, y1, x2, y2)
    const int x = frame.cols;
    if (box.at(0) < padding) {
        box.at(0) = padding;
    }
    if (box.at(2) > (x - padding)) {
        box.at(2) = (x - padding);
    }

    const int y = frame.rows;
    if (box.at(1) < padding) {
        box.at(1) = padding;
    }
    if (box.at(3) > (y - padding)) {
        box.at(3) = (y - padding);
    }
}

void UseCase::ShowImage(const Mat frame) {
    imshow("Frame", frame);
    imwrite("out.jpg", frame);
}
